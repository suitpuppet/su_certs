# Install a root certificate. Creates the hash symlink.

define su_certs::root(
  Enum['present', 'absent'] $ensure = 'present'
) {

  # Make sure the ca-certificates package is installed.
  ensure_packages(['ca-certificates', 'openssl'], {ensure => present})

  file { "/etc/ssl/certs/${name}.pem":
    ensure  => $ensure,
    source  => "puppet:///modules/su_certs/etc/ssl/certs/${name}.pem",
    require => $::osfamily ? {
      'Debian' => Package['ca-certificates'],
      'RedHat' => $::lsbmajdistrelease ? {
        '6'     => Package['ca-certificates'],
        default => Package['openssl'],
      },
    },
  }

  su_certs::hash { "${name}.pem": ensure => $ensure }
}
