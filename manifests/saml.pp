# Install a SAML certificate and key

# The key is downloaded via wallet.  The certificate will be deployed to
# /etc/shibboleth/saml-crt.pem and the key to /etc/shibboleth/saml-key.pem

# Syntax:
#
#     su_certs::saml { "<hostname>":
#         ensure      => present,
#         keyname     => "ssl-key/<hostname>",
#         identity    => "<hostname>",
#         symlink     => false,
#     }
#
# <hostname> MUST be full-qualified.
#
# Only ensure need be specified; the other listed parameters are the defaults.
# <hostname> should be the unqualified hostname.  

# This define does not notify any services (e.g., apache), so if you need to
# notify a service when there is a change you will have to do so yourself.

########################################################################################

# $name: This should be the FULLY qualified name. For example "example.stanford.edu". You can
# also provide something like "example.stanford.edu-saml" for a SAML key-pair. $name implies
# the wallet name and certificate filename:
#
#    wallet name -> "ssl-key/$name"
#    cert_files  -> $name
#
# Example. If $name is "example.stanford.edu-saml" then the wallet name will be
# "ssl-key/example.stanford.edu-saml" and the certificate pulled from the cert_files
# module will be "example.stanford.edu-saml".
#
# $ensure: set to present to install the certificate, absent to uninstall. This parameter
# is required and defaults to 'present'.
#
# $keyname: the Wallet file object name. Defaults to "ssl-key/$name"
#
# $owner: file owner of the private key. Defaults to "_shibd".
#
# $group: group owner of the private key. Defaults to "_shibd".
#
# $identity: If the file name in the cert_files module does NOT match $name, you can specify
# the file name with $identity. Example:
#
# su_certs::saml { 'example.stanford.edu':
#   identity => 'example2.stanford.edu',
# }
#
# This will install the certificate "example.stanford.edu" but pull use the certificate file
# "example2.stanford.edu" from the cert_files module.
#

########################################################################################

define su_certs::saml(
  Enum['present', 'absent'] $ensure     = 'present',
  Optional[String]          $keyname    = undef,
  String                    $owner      = '_shibd',
  String                    $group      = '_shibd',
  String                    $cert_path  = '/etc/shibboleth/saml-crt.pem',
  String                    $key_path   = '/etc/shibboleth/saml-key.pem',
  String                    $cert_mode  = '0644',
  String                    $key_mode   = '0600',
  Optional[String]          $identity   = undef,
) {

  # Install the private key using Wallet.
  case $keyname {
    undef:   { $key = "ssl-key/${name}" }
    default: { $key = $keyname }
  }

  wallet { $key:
    ensure  => $ensure,
    type    => 'file',
    path    => $key_path,
    owner   => $owner,
    group   => $group,
    mode    => $key_mode,
  }

  # Install the public certificate.
  file { $cert_path:
    ensure  => $ensure,
    source  => $identity ? {
      undef   => "puppet:///modules/cert_files/${name}",
      default => "puppet:///modules/cert_files/${identity}",
    },
    mode    => $cert_mode,
  }
}

